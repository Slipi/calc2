var op;
var num1;
var num2;
function addValue(num) {
    if (num == '-') {
        var str = document.getElementById("num1").value;
        if (str[0] == '-') {
            for (var i = 0; i < str.length - 1; i++)
                str = str.charAt(str.length - 1) + str.substr(0, str.length - 1);
            str = str.substr(0, str.length - 1);
            document.getElementById("num1").value = str;
        }
        else {
            str = '-' + str;
            document.getElementById("num1").value = str;
        }
    }
    else
        document.getElementById("num1").value = document.getElementById("num1").value + num;
}
function takeNum() {
    var str = document.getElementById("num1").value;
    if (isNaN(num1)) {
        num1 = +(str);
    }
    else if (isNaN(num2)) {
        num2 = +(str);
    }
}
function clearFunc() {
    document.getElementById("num1").value = "";
}
function func() {
    var result = "";
    if (isNaN(num1) || isNaN(num2)) {
        document.getElementById("num1").value = "";
        return;
    }
    switch (op) {
        case '+':
            result = String(num1 + num2);
            break;
        case '-':
            result = String(num1 - num2);
            break;
        case '*':
            result = String(num1 * num2);
            break;
        case '/':
            if (num2) {
                result = String(num1 / num2);
            }
            else {
                result = 'inf';
            }
            break;
        case 'C':
            num1 = NaN;
            num2 = NaN;
            result = "";
            break;
        default:
            result = 'operation?';
    }
    num1 = NaN;
    num2 = NaN;
    document.getElementById("num1").value = result;
}
